<?php

declare(strict_types=1);

use App\BootstrapConfigExample;

class_alias(BootstrapConfigExample::class, 'App\BootstrapConfig');
