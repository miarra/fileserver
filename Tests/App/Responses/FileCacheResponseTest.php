<?php declare(strict_types = 1);

namespace Tests\App\Responses;

require_once __DIR__ . '/../../Bootstrap.php';

use App\Responses\Context\Factory as ContextFactory;
use App\Responses\FileCache\Response as FileCacheResponse;
use Mockery;
use Nette\Http\Context;
use Nette\Http\IRequest as Request;
use Nette\Http\IResponse as Response;
use Nette\Utils\DateTime;
use Nette\Utils\Image;
use Nette\Utils\ImageException;
use Tester\Assert;
use Tester\Environment;
use Tester\FileMock;
use Tester\TestCase;
use Tests\Bootstrap;
use Throwable;
use function assert;
use function count;
use function file_get_contents;
use function ob_get_clean;
use function ob_start;
use function putenv;

Environment::bypassFinals();

class FileCacheResponseTest extends TestCase
{

	protected function setUp(): void
	{
		parent::setUp();
		putenv('MOD_X_SENDFILE_ENABLED=0');
	}

	protected function tearDown(): void
	{
		parent::tearDown();
		Mockery::close();
	}

	/**
	 * @dataProvider sendArgs
	 */
	public function testSend(
		FileCacheResponse $fileCacheResponse,
		string $expected,
		string|null $mimeType = null,
		string|null $filePath = null,
		bool $useXSendFile = false,
	): void
	{
		ob_start();
		$fileCacheResponse->send(
			$this->getRequestMock(),
			$this->getResponseMock($mimeType, $filePath, $useXSendFile),
		);
		Assert::same($expected, ob_get_clean());
	}

	/**
	 * @return array<array<mixed>>
	 * @throws ImageException
	 */
	public function sendArgs(): array
	{
		$contextFactoryMock = $this->getContextFactoryMock();

		$img1 = FileMock::create(extension: '/') . 'down2/down2.jpg';
		Image::fromBlank(1, 1)->save($img1);
		$mimeType = Image::typeToMimeType((int) Image::detectTypeFromFile($img1));
		$fileCacheResponse = new FileCacheResponse($contextFactoryMock, '2 minutes', $img1);

		return [
			[
				'fileCacheResponse' => $fileCacheResponse,
				'expected' => '',
			],
			[
				'fileCacheResponse' => $fileCacheResponse,
				'expected' => file_get_contents($img1),
			],
			[
				'fileCacheResponse' => $fileCacheResponse,
				'expected' => '',
				'mimeType' => $mimeType,
				'filePath' => $img1,
				'useXSendFile' => true,
			],
		];
	}

	private function getRequestMock(): Request
	{
		$retVal = Mockery::mock(Request::class);
		assert($retVal instanceof Request);

		return $retVal;
	}

	private function getResponseMock(
		string|null $mimeType = null,
		string|null $filePath = null,
		bool $useXSendFile = false,
	): Response
	{
		$allowedNamesValues = [
			'Pragma' => [0, ''],
			'Cache-Control' => [0, ''],
		];

		if ($useXSendFile) {
			putenv('MOD_X_SENDFILE_ENABLED=1');
			$allowedNamesValues['Content-Type'] = [0, $mimeType];
			$allowedNamesValues['X-Sendfile'] = [0, $filePath];
		}

		$retVal = Mockery::mock(Response::class)
			->shouldReceive('setHeader')
			->times(count($allowedNamesValues))
			->withArgs(static function (string $name, string $value) use (&$allowedNamesValues) {
				if (
						($allowedNamesValues[$name][0] ?? 1) ||
						$value !== $allowedNamesValues[$name][1]
					) {
					return false;
				}

					$allowedNamesValues[$name][0] = 1;

					return true;
			})
			->shouldReceive('setExpiration')
				->withArgs(static function (string|null $time = null) {
					if (!$time) {
						return true;
					}

					try {
						DateTime::from($time);

						return true;
					} catch (Throwable) {
						return false;
					}
				})
			->getMock();
		assert($retVal instanceof Response);

		return $retVal;
	}

	private function getContextFactoryMock(): ContextFactory
	{
		$contextMock = $this->getContextMock();
		$retVal = Mockery::mock(ContextFactory::class)
			->shouldReceive('create')
			->with(Mockery::type(Request::class), Mockery::type(Response::class))
			->andReturn($contextMock)
			->getMock();
		assert($retVal instanceof ContextFactory);

		return $retVal;
	}

	private function getContextMock(): Context
	{
		$retVal = Mockery::mock(Context::class)
			->shouldReceive('isModified')
			->withArgs(static fn (int|false $arg) => true)
			->andReturn(false, true)
			->getMock();
		assert($retVal instanceof Context);

		return $retVal;
	}

}

(new FileCacheResponseTest())->run();
Bootstrap::removeTempDir();
