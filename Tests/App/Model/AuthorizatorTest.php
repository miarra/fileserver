<?php declare(strict_types = 1);

namespace Tests\App\Model;

require_once __DIR__ . '/../../Bootstrap.php';

use App\Model\Authorizator;
use App\Model\Error;
use Mockery;
use Nette\Http\IRequest as Request;
use Nette\Http\IResponse as Response;
use ReflectionClass;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;
use Tests\Bootstrap;
use function array_fill;
use function count;

Environment::bypassFinals();

class AuthorizatorTest extends TestCase
{

	private string $username = 'username';

	private string $password = 'password';

	/**
	 * @dataProvider authenticateArgs
	 */
	public function testAuthenticate(Authorizator $authorizator, Error|null $expected): void
	{
		Assert::equal($expected, $authorizator->authenticate());
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function authenticateArgs(): array
	{
		$requestMock = $this->getRequestMockHeader();

		$authorizator = new Authorizator(
			$this->username,
			$this->password,
			$requestMock,
		);

		return [
			[
				'authorizator' => $authorizator,
				'expected' => null,
			],
			[
				'authorizator' => $authorizator,
				'expected' => Error::create(
					Response::S401_UNAUTHORIZED,
					'Wrong username or password',
				),
			],
			[
				'authorizator' => $authorizator,
				'expected' => Error::create(
					Response::S401_UNAUTHORIZED,
					'Wrong username or password',
				),
			],
			[
				'authorizator' => $authorizator,
				'expected' => Error::create(
					Response::S401_UNAUTHORIZED,
					'Wrong username or password',
				),
			],
			[
				'authorizator' => $authorizator,
				'expected' => Error::create(
					Response::S401_UNAUTHORIZED,
					'Wrong authorization method',
				),
			],
			[
				'authorizator' => $authorizator,
				'expected' => Error::create(
					Response::S401_UNAUTHORIZED,
					'Missing authorization',
				),
			],
		];
	}

	/**
	 * @dataProvider checkMethodArgs
	 */
	public function testCheckMethod(Authorizator $authorizator, string $method, Error|null $expected): void
	{
		Assert::equal($expected, $authorizator->checkMethod($method));
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function checkMethodArgs(): array
	{
		$requestMock = $this->getRequestMockIsMethod();
		$authorizator = new Authorizator('', '', $requestMock);

		$retVal = [
			[
				'authorizator' => $authorizator,
				'method' => Request::GET,
				'expected' => Error::create(
					Response::S405_METHOD_NOT_ALLOWED,
					'Wrong method please use ' . Request::GET,
				),
			],
			[
				'authorizator' => $authorizator,
				'method' => 'foo',
				'expected' => Error::create(
					Response::S405_METHOD_NOT_ALLOWED,
					'Wrong method please use foo',
				),
			],
			[
				'authorizator' => $authorizator,
				'method' => 'boo',
				'expected' => null,
			],
		];

		foreach ($this->getRequestConstants() as $value) {
			$retVal[] = [
				'authorizator' => $authorizator,
				'method' => $value,
				'expected' => null,
			];
		}

		return $retVal;
	}

	private function getRequestMockHeader(): Request
	{
		$retVal = [
			'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', // 'Basic '. base64_encode('username:password')
			'Basic bmFtZTpwYXNz', // 'Basic '. base64_encode('name:pass')
			'Basic dXNuYW1lOnBhc3M=', // 'Basic '. base64_encode('usname:pass')
			'Basic dXNlbmFtZTpwYXNzd3E=', // 'Basic '. base64_encode('usename:passwq')
			'BasicFoo dXNlcm5hbWU6cGFzc3dvcmQ=', // 'BasicFoo '. base64_encode('username:password')
			null,
		];

		return Mockery::mock(Request::class)
			->shouldReceive('getHeader')
			->with('authorization')
			->andReturn(...$retVal)
			->getMock();
	}

	private function getRequestMockIsMethod(): Request
	{
		$retVals = array_fill(0, count($this->getRequestConstants()) + 3, true);
		$retVals[0] = false;
		$retVals[1] = false;

		return Mockery::mock(Request::class)
			->shouldReceive('isMethod')
			->with(Mockery::type('string'))
			->andReturn(...$retVals)
			->getMock();
	}

	/**
	 * @return array<string>
	 */
	public function getRequestConstants(): array
	{
		return (new ReflectionClass(Request::class))->getConstants();
	}

}

(new AuthorizatorTest())->run();
Bootstrap::removeTempDir();
