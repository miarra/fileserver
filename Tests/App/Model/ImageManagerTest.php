<?php declare(strict_types = 1);

namespace Tests\App\Model;

require_once __DIR__ . '/../../Bootstrap.php';

use App\Model\ImageManager;
use Closure;
use Mockery;
use Nette\FileNotFoundException;
use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Schema\ValidationException;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\UnknownImageFileException;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;
use Tests\Bootstrap;
use function file_exists;

Environment::bypassFinals();

class ImageManagerTest extends TestCase
{

	private string $imagesPath;

	private ImageManager $imageManager;

	public function __construct()
	{
		$this->imagesPath = Bootstrap::getTempDir();//FileMock::create(extension: '/');//
		$this->imageManager = new ImageManager(pathPrefix: $this->imagesPath);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
				Bootstrap::purge();
		Mockery::close();
	}

	/**
	 * @dataProvider saveImagesArgs
	 * @param array<string> $filePaths
	 * @param array<string> $expected
	 */
	public function testSaveImages(
		callable $filesFunction,
		array $filePaths,
		array $expected,
	): void
	{
		Assert::same($expected, $this->imageManager->saveImages($filesFunction(), $filePaths));
	}

		/**
		 * @return array<int, array<string, array<mixed>>>
		 */
	public function saveImagesArgs(): array
	{
			$filePaths = [
				'a3fa8017c83dfded6c10de14f548bbfc' => 'img4.jpg',
				'00493b3f5af40f5e614214702e49cd8f' => '/img1.jpg',
				'2a9ea69d231c9bb94b88564d4ad20e08' => '/foo/img2.jpg',
				'05d0e7b29d64e56dc46ac86ef0afb3d7' => './../foo/../img3.jpg',
			];

			return [
				[
					'filesFunction' => [$this, 'getImagesForSaveCase4'],
					'filePaths' => $filePaths,
					'expected' => [
						'/img4.jpg',
					],
				],
				[
					'filesFunction' => [$this, 'getImagesForSaveCase1'],
					'filePaths' => $filePaths,
					'expected' => [
						'/img1.jpg',
					],
				],
				[
					'filesFunction' => [$this, 'getImagesForSaveCase2'],
					'filePaths' => $filePaths,
					'expected' => [
						'/img1.jpg',
						'/foo/img2.jpg',
					],
				],
				[
					'filesFunction' => [$this, 'getImagesForSaveCase3'],
					'filePaths' => $filePaths,
					'expected' => [
						'/img1.jpg',
						'/foo/img3.jpg',
					],
				],
			];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveCase1(): array
	{
		return [
			$this->getFileUploadMock('img1.jpg'),
		];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveCase2(): array
	{
		return [
			$this->getFileUploadMock('img1.jpg'),
			$this->getFileUploadMock('img2.jpg'),
		];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveCase3(): array
	{
		return [
			$this->getFileUploadMock('img1.jpg'),
			$this->getFileUploadMock('img3.jpg'),
		];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveCase4(): array
	{
		return [
			$this->getFileUploadMock('img4.jpg'),
		];
	}

	/**
	 * @dataProvider saveImagesTimestampArgs
	 * @param array<string> $filePaths
	 * @param array<string> $expected
	 */
	public function testSaveImagesTimestamp(
		callable $filesFunction,
		array $filePaths,
		array $expected,
	): void
	{
		$saveImagesRet = $this->imageManager->saveImages($filesFunction(), $filePaths);
				Assert::same($expected[0], $saveImagesRet[0]);
				Assert::truthy(Strings::match($saveImagesRet[1], $expected[1]));
	}

		/**
		 * @return array<array<string|array<mixed>>>
		 */
	public function saveImagesTimestampArgs(): array
	{
			return [
				[
					'filesFunction' => [$this, 'getImagesForSaveCase2'],
					'filePaths' => [
						'00493b3f5af40f5e614214702e49cd8f' => '/img1.jpg',
						'2a9ea69d231c9bb94b88564d4ad20e08' => '/img1.jpg',
					],
					'expected' => [
						'/img1.jpg',
						'~^\/img1\-[1-9]\d{0,13}\.jpg$~',
					],
				],
			];
	}

	/**
	 * @dataProvider saveImagesUnknownImageFileExceptionArgs
	 * @param array<string> $filePaths
	 */
	public function testSaveImagesUnknownImageFileException(
		callable $filesFunction,
		array $filePaths,
		string $errorFiles,
	): void
	{
			$imageManager = $this->imageManager;
			Assert::exception(static function () use ($imageManager, $filesFunction, $filePaths): void {
				$imageManager->saveImages($filesFunction(), $filePaths);
			}, UnknownImageFileException::class, 'File must be an image: ' . $errorFiles, 654_231);
	}

		/**
		 * @return  array<int, array<string, array<mixed>|string>>
		 */
	public function saveImagesUnknownImageFileExceptionArgs(): array
	{
			return [
				[//one image, bad type
					'filesFunction' => [$this, 'getImagesForSaveUnknownImageFileExceptionCase1'],
					'filePaths' => [
						'00493b3f5af40f5e614214702e49cd8f' => '/img1.jpg',
					],
					'errorFiles' => '"img1.jpg"',
				],
				[//two images, both bad type
					'filesFunction' => [$this, 'getImagesForSaveUnknownImageFileExceptionCase2'],
					'filePaths' => [
						'00493b3f5af40f5e614214702e49cd8f' => '/img1.jpg',
						'a3fa8017c83dfded6c10de14f548bbfc' => 'img4.jpg',
					],
					'errorFiles' => '"img1.jpg", "img4.jpg"',
				],
				[//two images, one bad type
					'filesFunction' => [$this, 'getImagesForSaveUnknownImageFileExceptionCase3'],
					'filePaths' => [
						'00493b3f5af40f5e614214702e49cd8f' => '/img1.jpg',
						'a3fa8017c83dfded6c10de14f548bbfc' => 'img4.jpg',
					],
					'errorFiles' => '"img4.jpg"',
				],
			];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveUnknownImageFileExceptionCase1(): array
	{
		return [
			$this->getOnlyNameFileUploadMock(imageName: 'img1.jpg', isImage: false),
		];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveUnknownImageFileExceptionCase2(): array
	{
		return [
			$this->getOnlyNameFileUploadMock(imageName: 'img1.jpg', isImage: false),
			$this->getOnlyNameFileUploadMock(imageName: 'img4.jpg', isImage: false),
		];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveUnknownImageFileExceptionCase3(): array
	{
		return [
			$this->getOnlyNameFileUploadMock(imageName: 'img1.jpg', times: 0, isImage: true),
			$this->getOnlyNameFileUploadMock(imageName: 'img4.jpg', isImage: false),
		];
	}

	/**
	 * @dataProvider saveImagesThrowsErrorArgs
	 * @param array<string> $filePaths
	 */
	public function testSaveImagesThrowsError(
		callable $filesFunction,
		array $filePaths,
	): void
	{
			$imageManager = $this->imageManager;
			Assert::exception(static function () use ($imageManager, $filesFunction, $filePaths): void {
				$imageManager->saveImages($filesFunction(), $filePaths);
			}, InvalidArgumentException::class, 'File name must be set. Use Miarra/FileManipulator.', 789_879);
	}

		/**
		 * @return array<int, array<string, array<mixed>>>
		 */
	public function saveImagesThrowsErrorArgs(): array
	{
			$filePaths1 = [
				'00493b3f5af40f5e614214702e49cd8f' => '',//empty $fileName
			];
			$filePaths2 = [
				'468044fda7335eead9c27ee9640ce478' => 'img1.jpg',//bad hash
			];

			return [
				[//empty $fileName
					'filesFunction' => [$this, 'getImagesForSaveThrowsErrorCase1'],
					'filePaths' => $filePaths1,
				],
				[//bad hash
					'filesFunction' => [$this, 'getImagesForSaveThrowsErrorCase1'],
					'filePaths' => $filePaths2,
				],
				[//empty paths
					'filesFunction' => [$this, 'getImagesForSaveThrowsErrorCase1'],
					'filePaths' => [],
				],
			];
	}

		/**
		 * @return array<FileUpload>
		 */
	public function getImagesForSaveThrowsErrorCase1(): array
	{
		return [
			$this->getOnlyNameFileUploadMock('img1.jpg'),
		];
	}

	/**
	 * @dataProvider getImageArgs
	 * @param int $width
	 * @param int $height
	 * @param string $expected
	 */
	public function testGetImage(
		string $fileName,
		string $originalFilePath,
		int|null $width = null,
		int|null $height = null,
		string|null $expected = null,
	): void
	{
		FileSystem::write($originalFilePath, '1');
				Image::fromBlank(width: 1, height: 1)->save($originalFilePath);

		Assert::same($expected, $this->imageManager->getImage(
			fileName: $fileName,
			width: $width,
			height: $height,
		));
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function getImageArgs(): array
	{
				$down1Folder = $this->imagesPath . '/down1';
				$down3Folder = $this->imagesPath . '/down3';
				$down3aFolder = $this->imagesPath . '/a/down3';

		return [
			[
				'fileName' => '/down1.jpg',
				'originalFilePath' => $down1Folder . '/down1.jpg',
				'expected' => $down1Folder . '/down1.jpg',
			],
			[
				'fileName' => 'down3.jpg',
				'width' => 560,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => $down3Folder . '/down3_w560.jpg',
			],
			[
				'fileName' => 'down3.jpg',
				'height' => 1_234,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => $down3Folder . '/down3_h1234.jpg',
			],
			[
				'fileName' => 'down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => $down3Folder . '/down3_w560_h1234.jpg',
			],
			[
				'fileName' => '/../a/down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w560_h1234.jpg',
			],
			[
				'fileName' => '/../a/../../down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w560_h1234.jpg',
			],
			[
				'fileName' => '/../a/.././down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w560_h1234.jpg',
			],
			[
				'fileName' => '/../a/././down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w560_h1234.jpg',
			],
			[
				'fileName' => '/../a/././down3.jpg',
				'width' => -1,
				'height' => 1_234,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w-1_h1234.jpg',
			],
			[
				'fileName' => '/../a/././down3.jpg',
				'width' => 560,
				'height' => -23,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w560_h-23.jpg',
			],
			[
				'fileName' => '/../a/././down3.jpg',
				'width' => -1,
				'height' => -15,
				'originalFilePath' => $down3aFolder . '/down3.jpg',
				'expected' => $down3aFolder . '/down3_w-1_h-15.jpg',
			],
			[
				'fileName' => '/../a/../../down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => null,
			],
			[
				'fileName' => '/../a/.././down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => null,
			],
			[
				'fileName' => '/./a/././down3.jpg',
				'width' => 560,
				'height' => 1_234,
				'originalFilePath' => $down3Folder . '/down3.jpg',
				'expected' => null,
			],
		];
	}

	/**
	 * @dataProvider getImageUnknownImageArgs
	 */
	public function testGetImageUnknownImage(
		string $fileName,
		string $originalFilePath,
		string $expected,
		int|null $width = null,
		int|null $height = null,
	): void
	{
		FileSystem::write($originalFilePath, '1');

				$imageManager = $this->imageManager;

				Assert::exception(static function () use ($imageManager, $fileName, $width, $height): void {
					$imageManager->getImage(
						fileName: $fileName,
						width: $width,
						height: $height,
					);
				}, UnknownImageFileException::class, $expected);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function getImageUnknownImageArgs(): array
	{
				$down1Folder = $this->imagesPath . '/down1';

		return [
			[//original file doesn't exists
				'fileName' => '/down1.jpg',
				'width' => 560,
				'originalFilePath' => $down1Folder . '/down1.jpg',
				'expected' => 'Unknown type of file \'' . $down1Folder . '/down1.jpg\'.',
			],
		];
	}

	/**
	 * @dataProvider getImageInvalidArgumentArgs
	 */
	public function testGetImageInvalidArgument(
		string $fileName,
		int|null $width = null,
		int|null $height = null,
	): void
	{
		$imageManager = $this->imageManager;

				Assert::exception(static function () use ($imageManager, $fileName, $width, $height): void {
					$imageManager->getImage(
						fileName: $fileName,
						width: $width,
						height: $height,
					);
				}, InvalidArgumentException::class, 'File name must be set. Use Miarra/FileManipulator.', 789_879);
	}

	/**
	 * @return array<array<string|int>>
	 */
	public function getImageInvalidArgumentArgs(): array
	{
				return [
					[//no fileName
						'fileName' => '',
					],
					[//no fileName
						'fileName' => '',
						'width' => 560,
					],
					[//no fileName
						'fileName' => '',
						'width' => 560,
						'height' => 640,
					],
				];
	}

	/**
	 * @dataProvider deleteImageArgs
	 * @param array<string> $fileNames
	 * @param array<string> $expected
	 * @param array<string> $originalFolderPaths
	 * @param array<string> $usageCountContents
	 */
	public function testDeleteImage(
		array $fileNames,
		array $expected,
		array $originalFolderPaths,
		array $usageCountContents,
	): void
	{
			FileSystem::write($originalFolderPaths[0] . '/usage_count.txt', $usageCountContents[0]);
			FileSystem::write($originalFolderPaths[1] . '/usage_count.txt', $usageCountContents[1]);

			Assert::same($expected, $this->imageManager->deleteImages($fileNames));
			Assert::true(!file_exists($originalFolderPaths[0]), 'File was`n deleted and should be. line: ' . __LINE__);
			Assert::true(file_exists($originalFolderPaths[1]), 'File was deleted and shouldn`t be. line: ' . __LINE__);
			Assert::true(
				FileSystem::read($originalFolderPaths[1] . '/usage_count.txt') === '1',
				'File content is bad. line: ' . __LINE__,
			);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function deleteImageArgs(): array
	{
				$down1Folder = $this->imagesPath . '/down1';
				$down3Folder = $this->imagesPath . '/down3';

		return [
			[
				'fileNames' => [
					'/down1.jpg',
					'down3.jpg',
				],
				'expected' => [
					'/down1.jpg',
					'/down3.jpg',

				],
				'originalFolderPaths' => [
					$down1Folder,
					$down3Folder,
				],
				'usageCountContents' => [
					'1',
					'2',
				],
			],
		];
	}

	/**
	 * @dataProvider copyImageArgs
	 * @param array<string> $fileNames
	 * @param array<string> $expected
	 * @param array<string> $originalFolderPaths
	 * @param array<string> $usageCountContents
	 */
	public function testCopyImage(
		array $fileNames,
		array $expected,
		array $originalFolderPaths,
		array $usageCountContents,
	): void
	{
			FileSystem::write($originalFolderPaths[0], $usageCountContents[0]);
			FileSystem::write($originalFolderPaths[1], $usageCountContents[1]);
			FileSystem::write($originalFolderPaths[2], $usageCountContents[2]);

			Assert::same($expected, $this->imageManager->copyImages($fileNames));
			Assert::true(FileSystem::read($originalFolderPaths[0]) === '2');
			Assert::true(FileSystem::read($originalFolderPaths[1]) === '3');
			Assert::true(FileSystem::read($originalFolderPaths[2]) === '2');
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function copyImageArgs(): array
	{
				$down1Folder = $this->imagesPath . '/down1/usage_count.txt';
				$down2Folder = $this->imagesPath . '/down2/usage_count.txt';
				$down3Folder = $this->imagesPath . '/down3/usage_count.txt';

		return [
			[
				'fileNames' => [
					'/down1.jpg',
					'down3.jpg',
				],
				'expected' => [
					'/down1.jpg',
					'/down3.jpg',

				],
				'originalFolderPaths' => [
					$down1Folder,
					$down3Folder,
					$down2Folder,
				],
				'usageCountContents' => [
					'1',
					'2',
					'2',
				],
			],
		];
	}

	/**
	 * @dataProvider copyDeleteImageValidationExceptionArgs
	 * @param array<string> $fileNames
	 */
	public function testCopyDeleteImageValidationException(
		array $fileNames,
	): void
	{
			$imageManager = $this->imageManager;

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->deleteImages($fileNames);
			}, ValidationException::class);

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->copyImages($fileNames);
			}, ValidationException::class);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function copyDeleteImageValidationExceptionArgs(): array
	{
				return [
					[//bad array scheme
						'fileNames' => [
							1,
							'down3.jpg',
						],
					],
				];
	}

	/**
	 * @dataProvider copydeleteImageInvalidArgumentExceptionArgs
	 * @param array<string> $fileNames
	 */
	public function testCopyDeleteImageInvalidArgumentException(
		array $fileNames,
	): void
	{
			$imageManager = $this->imageManager;

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->deleteImages($fileNames);
			}, InvalidArgumentException::class, 'File name must be set. Use Miarra/FileManipulator.', 789_879);

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->copyImages($fileNames);
			}, InvalidArgumentException::class, 'File name must be set. Use Miarra/FileManipulator.', 789_879);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function copydeleteImageInvalidArgumentExceptionArgs(): array
	{
				return [
					[//fileName doesn't exists
						'fileNames' => [
							'',
							'down3.jpg',
						],
					],
				];
	}

	/**
	 * @dataProvider copyDeleteImageFileNotFoundExceptionArgs
	 * @param array<string> $fileNames
	 */
	public function testCopyDeleteImageFileNotFoundException(
		array $fileNames,
	): void
	{
			$imageManager = $this->imageManager;

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->deleteImages($fileNames);
			}, FileNotFoundException::class);

			Assert::exception(static function () use ($imageManager, $fileNames): void {
				$imageManager->copyImages($fileNames);
			}, FileNotFoundException::class);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function copyDeleteImageFileNotFoundExceptionArgs(): array
	{
				return [
					[//file doesn't exists
						'fileNames' => [
							'down3.jpg',
						],
					],
				];
	}

	private function getOnlyNameFileUploadMock(string $imageName, int $times = 1, bool $isImage = true): FileUpload
	{
		return Mockery::mock(FileUpload::class)
			->shouldReceive('getUntrustedName')
			->with()
			->times($times)
			->andReturn($imageName)
			->shouldReceive('isImage')
			->with()
			->once()
			->andReturn($isImage)
			->getMock();
	}

	private function getFileUploadMock(string $imageName, int $times = 1, bool $isImage = true): FileUpload
	{
		return Mockery::mock(FileUpload::class)
			->shouldReceive('getUntrustedName')
			->with()
			->times($times)
			->andReturn($imageName)
			->shouldReceive('move')
			->withArgs(Closure::fromCallable([$this, 'makeFile']))
			->once()
			->andReturnNull()
			->shouldReceive('isImage')
			->with()
			->once()
			->andReturn($isImage)
			->getMock();
	}

	public function makeFile(string $dest): bool
	{
		FileSystem::write($dest, 'dsf');

		return true;
	}

}

(new ImageManagerTest())->run();
Bootstrap::removeTempDir();
