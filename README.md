# Quickstart
This aplication stores images for others aplications.

## Setting
You must set options in local config file like this:
```php
parametres:
	username: 'username'
	password: 'password'
	cacheTime: '2 minutes'
```

Is recommended to set up VirtualHost like this (for Apache2):
```
<VirtualHost *:80>
        # informations about webmaster showed in case that web crasched
        ServerAdmin name@mail.foo
        # domain to aply rules
        ServerName domain.bar
        # set document root
        DocumentRoot /path/to/document/root
        # default setting for document root
        <Directory "/path/to/document/root/">
				AllowOverride All
				Order Allow,Deny
				Allow from all
				Require all granted
					
				#is recommended to use XSendFile
				<IfModule mod_xsendfile.c>
						<Files *.php>
								XSendFile On
								XSendFilePath /path/to/images/folder/
						</Files>
				</IfModule>
				
				#MOD_X_SENDFILE_ENABLED will be 1 if XSendFile is enabled, 0 otherwise
				SetEnv MOD_X_SENDFILE_ENABLED 0
				
	</Directory>
</VirtualHost>
```

Folders `log`, `temp` and `images` must have right to write.

# Saving and Loading files/images
For save, load and delete image use `miarra/filemanipulator` which you can find
on packagist `https://packagist.org/packages/miarra/filemanipulator` or on bitbucked `https://bitbucket.org/miarra/filemanipulator.git`.

# Security
All files are accessable only from given URL. Direct access is not permited. 

In case you don't use PROXY, you need concatenate returned URL with your file server URL and server images folder

(Example: 'https://< yourFileServerURL.xy >/images/< yourReturnedURL >').

Use Https protocol only.

## Conclusion
This Aplication runs with PHP 8.0 and Nette 3.1 and it is property of Miarra © 2021