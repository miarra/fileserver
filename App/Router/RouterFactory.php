<?php declare(strict_types = 1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{

	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList();

		$router->addRoute('images/save/', 'Homepage:saveImages');
		$router->addRoute('images/delete/', 'Homepage:deleteImages');
		$router->addRoute('images/copy/', 'Homepage:copyImages');
		$router->addRoute('images/<fileName .+>', 'Public:getImage');
		$router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');

		return $router;
	}

}
