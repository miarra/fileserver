<?php declare(strict_types = 1);

namespace App\Responses\FileCache;

interface Factory
{

	public function create(string $file): Response;

}
