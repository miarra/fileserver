<?php declare(strict_types = 1);

namespace App\Responses\FileCache;

use App\Responses\Context\Factory as ContextFactory;
use Nette\Application\Response as AResponse;
use Nette\Http\IRequest as HttpRequest;
use Nette\Http\IResponse as HttpResponse;
use Nette\Utils\Image;
use function filemtime;
use function getenv;

final class Response implements AResponse
{

	public function __construct(
		private ContextFactory $contextFactory,
		private string $cacheTime,
		private string $file,
	)
	{
	}

	public function send(HttpRequest $httpRequest, HttpResponse $httpResponse): void
	{
		$httpResponse->setHeader('Pragma', '');
		$httpResponse->setHeader('Cache-Control', '');

		$httpResponse->setExpiration($this->cacheTime);

		$context = $this->contextFactory->create($httpRequest, $httpResponse);

		$mTime = filemtime($this->file);
		if ($context->isModified((int) $mTime)) {
			$type = (int) Image::detectTypeFromFile($this->file);
			if (getenv('MOD_X_SENDFILE_ENABLED')) {
				$httpResponse->setHeader('Content-Type', Image::typeToMimeType($type));
				$httpResponse->setHeader('X-Sendfile', $this->file);
			} else {
				Image::fromFile($this->file)->send($type);
			}
		}
	}

}
