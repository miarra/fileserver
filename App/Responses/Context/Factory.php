<?php declare(strict_types = 1);

namespace App\Responses\Context;

use Nette\Http\Context;
use Nette\Http\IRequest as Request;
use Nette\Http\IResponse as Response;

interface Factory
{

	public function create(Request $request, Response $response): Context;

}
