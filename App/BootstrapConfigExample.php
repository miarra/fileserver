<?php declare(strict_types = 1);

namespace App;

class BootstrapConfigExample
{

	/**
	 * @return array<string>
	 */
	public static function getConfig(): array
	{
		$ips = [
			'userName1' => [
				'123.123.123.123',
			],
			'userName2' => [
				'89.89.89.89',
				'124.54.45.45',
			],
		];
		$keys = [
			'userName1' => [
				'insert_cookie_value_1',
			],
			'userName2' => [
				'insert_cookie_value_2',
				'insert_cookie_value_3',
			],
		];

		// <klic>@<IP>

		$retArray = [];

		foreach ($keys as $key => $cookies) {
			foreach ($ips[$key] as $ip) {
				foreach ($cookies as $cookie) {
					$retArray[] = $cookie . '@' . $ip;
				}
			}
		}

		return $retArray;
	}

}
