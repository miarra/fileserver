<?php declare(strict_types = 1);

namespace App\Model;

use Nette\SmartObject;

/**
 * Class Error
 *
 * @property int $code
 * @property string $message
 */
class Error
{

	use SmartObject;

	private int $c;

	private string $m;

	public static function create(int $code, string $message): self
	{
		$retVal = new self();

		$retVal->code = $code;
		$retVal->message = $message;

		return $retVal;
	}

	public function setCode(int $value): void
	{
		$this->c = $value;
	}

	public function getCode(): int
	{
		return $this->c;
	}

	public function setMessage(string $value): void
	{
		$this->m = $value;
	}

	public function getMessage(): string
	{
		return $this->m;
	}

}
