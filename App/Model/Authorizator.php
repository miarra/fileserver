<?php declare(strict_types = 1);

namespace App\Model;

use Nette\Http\IRequest as Request;
use Nette\Http\IResponse as Response;
use function base64_decode;
use function explode;

class Authorizator
{

	public function __construct(
		private string $username,
		private string $password,
		private Request $request,
	)
	{
	}

	public function authenticate(): Error|null
	{
		$myUsername = $this->username;
		$myPassword = $this->password;

		if (!($auth = ($this->request->getHeader('authorization') ?? null))) {
			return Error::create(Response::S401_UNAUTHORIZED, 'Missing authorization');
		}

		[$loginMethod, $base64] = explode(' ', $auth);

		switch ($loginMethod) {
			case 'Basic':
				[$login, $password] = explode(':', (string) base64_decode($base64, true));
				if (!($login === $myUsername && $password === $myPassword)) {
					return Error::create(Response::S401_UNAUTHORIZED, 'Wrong username or password');
				}

				break;
			default:
				return Error::create(Response::S401_UNAUTHORIZED, 'Wrong authorization method');
		}

		return null;
	}

	public function checkMethod(string $method): Error|null
	{
		if ($this->request->isMethod($method)) {
			return null;
		}

		return Error::create(Response::S405_METHOD_NOT_ALLOWED, 'Wrong method please use ' . $method);
	}

}
