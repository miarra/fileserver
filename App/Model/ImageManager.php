<?php declare(strict_types = 1);

namespace App\Model;

use Nette\FileNotFoundException;
use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Schema\Expect;
use Nette\Schema\Processor;
use Nette\Schema\ValidationException;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\UnknownImageFileException;
use function assert;
use function explode;
use function file_exists;
use function implode;
use function md5;
use function microtime;
use function str_replace;

//use Exception;

class ImageManager
{

	private const
				FILE_NOT_FOUND_MESSAGE_PREFIX = 'One or multiple files not found: ',
				USAGE_COUNT_FILE_NAME = '/usage_count.txt';

	public function __construct(private string $pathPrefix,)
	{
	}

	/**
	 * @param array<FileUpload> $files
	 * @param array<string> $filePaths
	 * @return array<string>
	 * @throws InvalidArgumentException
	 */
	public function saveImages(array $files, array $filePaths): array
	{
		$paths = [];

				$filesWithError = [];

		foreach ($files as $file) {
			if (!$file->isImage()) {
					$filesWithError[] = '"' . $file->getUntrustedName() . '"';
			}
		}

		if (!empty($filesWithError)) {
			throw new UnknownImageFileException(
				'File must be an image: ' . implode(', ', $filesWithError),
				654_231,
			);
		}

		foreach ($files as $file) {
			assert($file instanceof FileUpload);
			[
				'url' => $urlPath,
				'originalDisc' => $discPath,
				'usageCountFilePath' => $usageCountFilePath,
			] = $this->getPaths(fileName: $filePaths[md5($file->getUntrustedName())] ?? '', sameFileModify: true);

			$file->move($discPath);

						FileSystem::write($usageCountFilePath, '1');
			$paths[] = $urlPath;
		}

		return $paths;
	}

	/**
	 * @throws UnknownImageFileException
	 * @throws InvalidArgumentException
	 */
	public function getImage(
		string $fileName,
		int|null $width = null,
		int|null $height = null,
	): string|null
	{
		[
			'originalDisc' => $originalDiscPath,
			'concreteDisc' => $discPath,
		] = $this->getPaths($fileName, $width, $height);

		if (!file_exists($originalDiscPath)) {
			return null;
		}

		if (!file_exists($discPath)) {
			$image = Image::fromFile($originalDiscPath);
			$image->resize($width, $height);
			$image->save($discPath, 100, Image::detectTypeFromFile($originalDiscPath));
		}

		return $discPath;
	}

		/**
		 * @param array<string> $fileNames
		 * @return array<string>
		 * @throws ValidationException
		 * @throws FileNotFoundException
		 */
	public function deleteImages(array $fileNames): array
	{
				[
					'folderPaths' => $folderPaths,
					'urlPaths' => $urlPaths,
					'usageCountFilePaths' => $usageCountFilePaths,
				] = $this->processCopyDelete($fileNames);

				foreach ($usageCountFilePaths as $key => $usageCountFilePath) {
					if (($usageCount = (int) FileSystem::read($usageCountFilePath)) > 1) {
						FileSystem::write($usageCountFilePath, (string) ($usageCount - 1));
					} else {
						FileSystem::delete($folderPaths[$key]);
					}
				}

				return $urlPaths;
	}

		/**
		 * @param array<string> $fileNames
		 * @return array<string>
		 * @throws ValidationException
		 * @throws FileNotFoundException
		 */
	public function copyImages(array $fileNames): array
	{
				[
					'urlPaths' => $urlPaths,
					'usageCountFilePaths' => $usageCountFilePaths,
				] = $this->processCopyDelete($fileNames);

				foreach ($usageCountFilePaths as $usageCountFilePath) {
					$usageCount = (int) FileSystem::read($usageCountFilePath);
					FileSystem::write($usageCountFilePath, (string) ($usageCount + 1));
				}

				return $urlPaths;
	}

		/**
		 * @param array<string> $fileNames
		 * @return array<array<string>>
		 * @throws ValidationException
		 * @throws FileNotFoundException
		 */
	private function processCopyDelete(array $fileNames): array
	{
			$schema = Expect::arrayOf(Expect::string());
			$processor = new Processor();
			$processor->process($schema, $fileNames);

			$folderPaths = [];
			$urlPaths = [];
			$usageCountFilePaths = [];

			$filesNotFounds = [];

		foreach ($fileNames as $key => $fileName) {
			[
				'folderPath' => $folderPaths[$key],
				'url' => $urlPaths[$key],
				'usageCountFilePath' => $usageCountFilePaths[$key],
			] = $this->getPaths($fileName);

			//                        $filesNotFounds[] = '"' . $folderPaths[$key] . '"';
			//                        $filesNotFounds[] = '"' . $urlPaths[$key] . '"';
			//                        $filesNotFounds[] = '"' . $usageCountFilePaths[$key] . '"';
			if (!file_exists($usageCountFilePaths[$key])) {
				$filesNotFounds[] = '"' . $fileName . '"';
			}
		}

		if (!empty($filesNotFounds)) {
			throw new FileNotFoundException(
				self::FILE_NOT_FOUND_MESSAGE_PREFIX . implode(', ', $filesNotFounds),
				129_129,
			);
		}

			return [
				'folderPaths' => $folderPaths,
				'urlPaths' => $urlPaths,
				'usageCountFilePaths' => $usageCountFilePaths,
			];
	}

	/**
	 * @return array<string>
	 * @throws InvalidArgumentException
	 */
	private function getPaths(
		string $fileName,
		int|null $width = null,
		int|null $height = null,
		bool $sameFileModify = false,
	): array
	{
		if (!$fileName) {
			throw new InvalidArgumentException('File name must be set. Use Miarra/FileManipulator.', 789_879);
		}

				$fileName = str_replace('%20', ' ', $fileName);

				$fileName = $this->normalizeFileName($fileName);

				[
					$prefix,
					$suffix,
				] = explode('.', $fileName);

				$name = Strings::after($prefix, '/', -1);
				$folderPath = $this->pathPrefix . $prefix;
				$timestamp = '';

				if ($sameFileModify && file_exists($folderPath)) {
					$timestamp = '-' . str_replace('.', '', (string) microtime(true));
					$name .= $timestamp;
					$folderPath .= $timestamp;
				}

				$discPathPrefix = $folderPath . '/' . $name;
				$originalDiscPath = $discPathPrefix . '.' . $suffix;

				if ($width) {
					$discPathPrefix .= '_w' . $width;
				}

				if ($height) {
					$discPathPrefix .= '_h' . $height;
				}

				$discPath = $discPathPrefix . '.' . $suffix;

				$urlPath = $prefix . $timestamp . '.' . $suffix;

				return [
					'prefix' => $prefix,
					'suffix' => $suffix,
					'url' => str_replace(' ', '%20', str_replace('//', '/', $urlPath)),
					'originalDisc' => $originalDiscPath,
					'concreteDisc' => $discPath,
					'folderPath' => $folderPath,
					'usageCountFilePath' => $folderPath . self::USAGE_COUNT_FILE_NAME,
				];
	}

	private function normalizeFileName(string $fileName): string
	{
		$fileName = Strings::replace($fileName, '~\.?\.?(\/\.?\.?)+~', '/');

		if ($fileName[0] !== '/') {
			$fileName = '/' . $fileName;
		}

		return $fileName;
	}

}
