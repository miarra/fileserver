<?php declare(strict_types = 1);

namespace App\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\Request;
use Nette\Application\UI\Presenter;

final class Error4xxPresenter extends Presenter
{

	public function startup(): void
	{
		parent::startup();
		if (!$this->getRequest()?->isMethod(Request::FORWARD)) {
			$this->error();
		}
	}

	public function renderDefault(BadRequestException $exception): void
	{
		$this->sendJson([
			'state' => 'ERROR',
			'code' => $exception->getCode(),
			'message' => $exception->getMessage(),
		]);
	}

}
