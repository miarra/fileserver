<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Responses\FileCache\Factory as FileCacheResponseFactory;
use Nette\DI\Attributes\Inject;
use Nette\Http\Response;

final class PublicPresenter extends BasePresenter
{

	#[Inject]
	public FileCacheResponseFactory $fileCacheResponseFactory;

	public function actionGetImage(string $fileName, int|null $width = null, int|null $height = null): void
	{
		$filePath = $this->imageManager->getImage($fileName, $width, $height);

		if (!$filePath) {
			$this->error('File not found', Response::S404_NOT_FOUND);
		}

		$resp = $this->fileCacheResponseFactory->create($filePath);

		$this->sendResponse($resp);
	}

}
