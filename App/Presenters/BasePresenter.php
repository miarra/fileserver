<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\Error;
use App\Model\ImageManager;
use Nette\Application\AbortException;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;

abstract class BasePresenter extends Presenter
{

	#[Inject]
	public ImageManager $imageManager;

	protected function sendError(Error $error): void
	{
		$this->getHttpResponse()->setCode($error->code);
		$this->sendJson([
			'state' => 'ERROR',
			'code' => $error->code,
			'message' => $error->message,
		]);
	}

	/**
	 * @param array<string> $filePaths
	 * @throws AbortException
	 */
	protected function sendFilePaths(array $filePaths): void
	{
		$this->sendJson([
			'state' => 'SUCCESS',
			'filepath' => $filePaths,
		]);
	}

}
