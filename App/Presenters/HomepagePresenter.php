<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\Authorizator;
use App\Model\Error;
use Nette\Application\Request;
use Nette\Application\Response;
use Nette\Application\Responses\JsonResponse;
use Nette\DI\Attributes\Inject;
use Nette\FileNotFoundException;
use Nette\Http\FileUpload;
use Nette\Schema\ValidationException;
use Nette\Utils\Json;
use Nette\Utils\UnknownImageFileException;
use Throwable;
use Tracy\Debugger;
use function file_get_contents;

final class HomepagePresenter extends BasePresenter
{

	private const
				IMAGE_REGEX = '/.*\.(gif|jpe?g|bmp|png|webp)$/';

	#[Inject]
	public Authorizator $authorizator;

	protected function startup(): void
	{
		parent::startup();
		if ($isAuthenticated = $this->authorizator->authenticate()) {
			$this->sendError($isAuthenticated);
		}
	}

	public function run(Request $request): Response
	{
		try {
			return parent::run($request);
		} catch (Throwable $ex) {
			Debugger::log($ex);
			$this->getHttpResponse()->setCode($this->getHttpResponse()::S500_INTERNAL_SERVER_ERROR);

			return new JsonResponse([
				'state' => 'ERROR',
				'code' => $this->getHttpResponse()::S500_INTERNAL_SERVER_ERROR,
				'message' => 'INTERNAL ERROR OCCURES',
			]);
		}
	}

	public function actionSaveImages(): void
	{
		$this->checkMethod($this->getHttpRequest()::POST);
				$files = $this->getFiles();
				$filePaths = $this->getHttpRequest()->getPost();
		try {
			$this->sendFilePaths($this->imageManager->saveImages($files, $filePaths));
		} catch (UnknownImageFileException $ex) {
			$this->sendError(
				Error::create(
					$this->getHttpResponse()::S422_UNPROCESSABLE_ENTITY,
					$ex->getMessage(),
				),
			);
		}
	}

	public function actionDeleteImages(): void
	{
			$this->processCopyDelete($this->getHttpRequest()::DELETE, 'deleteImages');
	}

	public function actionCopyImages(): void
	{
			$this->processCopyDelete($this->getHttpRequest()::POST, 'copyImages');
	}

	private function processCopyDelete(string $method, string $functionName): void
	{
		$this->checkMethod($method);
				$fileContents = file_get_contents('php://input');
		$fileNames = Json::decode($fileContents ? $fileContents : Json::encode([]));
		try {
			$paths = $this->imageManager->$functionName($fileNames);
		} catch (ValidationException) {
			$this->sendError(
				Error::create(
					$this->getHttpResponse()::S422_UNPROCESSABLE_ENTITY,
					'Param $fileNames must be array of strings. Use Miarra/FileManipulator.',
				),
			);
		} catch (FileNotFoundException $ex) {
			$this->sendError(
				Error::create(
					$this->getHttpResponse()::S404_NOT_FOUND,
					$ex->getMessage(),
				),
			);
		}

			$this->sendFilePaths($paths);
	}

	private function checkMethod(string $method): void
	{
		if ($isAuthenticated = $this->authorizator->checkMethod($method)) {
			$this->sendError($isAuthenticated);
		}
	}

		/**
		 * @return array<FileUpload>
		 */
	private function getFiles(): array
	{
		return $this->getHttpRequest()->getFiles();
	}

}
